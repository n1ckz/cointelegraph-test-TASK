# Cointelegraph test TASK

You have a book with some pages. 
Write the class PaginationHelper and it’s constructor, implements the following methods:

1. In constructor setup array of items and count of items on page
2. Method that print count of pages for entire collection of items
3. Count of items
4. Items count on page
5. Print page number where item is located 
6. Write 2 variants of the code in strict mode and without it.
7. Write the tests with CHAI/MOCHA (advanced, is a plus)

Your class should pass this tests:

```js
    var helper = new PaginationHelper(['a','b','c','d','e','f'], 4);
    helper.pageCount() // should == 2
    helper.itemCount() // should == 6
    helper.pageItemCount(0); //should == 4
    helper.pageItemCount(1) // last page - should == 2
    helper.pageItemCount(2) // should == -1 since the page is invalid

    helper.pageIndex(5) // should == 1 (zero based index)
    helper.pageIndex(2) // should == 0
    helper.pageIndex(20) // should == -1
    helper.pageIndex(-10) // should == -1
```

You can check out it here: https://repl.it/LPtH





